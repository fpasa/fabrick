#include "uik.h"

struct uik_label* uik_label_new(char* name, char* text) {
    struct uik_label* label = malloc(sizeof(struct uik_label));

    label->widget.class = LABEL;
    label->widget.parent = NULL;
    label->widget.children = NULL;

    label->text = text;

    uik_add_obj(name, label);

    return label;
}

struct _uik_geometry _uik_label_layout_size(struct uik_label* label, struct _uik_constrains* constrains) {
    struct _uik_geometry geo = {
        .width = constrains->min_width,
        .height = constrains->min_height,
    };
    return geo;
}

void _uik_label_layout_position(struct uik_label* label) {}

void _uik_label_render((struct uik_label*)widget) {
    
}