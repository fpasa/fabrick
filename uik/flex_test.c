#include "test/minunit.h"

#include "uik.h"

char* test_uik_flex_new() {
    struct uik_flex* flex = uik_flex_new("flex", HORIZONTAL);

    mu_assert("created flex widget has class 'flex'",
        flex->widget.class == FLEX);

    mu_assert("created flex widget has correct orientation",
        flex->orientation == HORIZONTAL);

    struct uik_flex* reg_flex = NULL;
    uik_get_obj("flex", (void **)&reg_flex);
    mu_assert("created flex widget is added to the registry",
        reg_flex == flex);

    return NULL;
}

char* test_uik_flex_layout_no_extend() {
    struct uik_window* win = uik_window_new("win", 640, 480, 0, 0);

    struct uik_flex* flex = uik_flex_new("flex", HORIZONTAL);
    uik_window_pack(win, (void*)flex);

    struct uik_label* label1 = uik_label_new("label1", "test1");
    uik_flex_pack(flex, (void*)label1, 0);
    struct uik_label* label2 = uik_label_new("label2", "test2");
    uik_flex_pack(flex, (void*)label2, 0);

    _uik_layout((void*)win);

    mu_assert("Children have original size (1w)", label1->widget.width == 0);
    mu_assert("Children have original size (1h)", label1->widget.height == 0);
    mu_assert("Children have original size (2w)", label2->widget.width == 0);
    mu_assert("Children have original size (2h)", label2->widget.height == 0);

    mu_assert("Children are positioned on the top-left corner (1w)",
        label1->widget.x == 0);
    mu_assert("Children are positioned on the top-left corner (1h)",
        label1->widget.y == 0);
    mu_assert("Children are positioned on the top-left corner (2w)",
        label2->widget.x == 0);
    mu_assert("Children are positioned on the top-left corner (2h)",
        label2->widget.y == 0);

    return NULL;
}

char* test_uik_flex_layout_extend() {
    struct uik_window* win = uik_window_new("win", 600, 400, 0, 0);

    struct uik_flex* flex = uik_flex_new("flex", HORIZONTAL);
    uik_window_pack(win, (void*)flex);

    struct uik_label* label1 = uik_label_new("label1", "test1");
    uik_flex_pack(flex, (void*)label1, 1);
    struct uik_label* label2 = uik_label_new("label2", "test2");
    uik_flex_pack(flex, (void*)label2, 2);

    _uik_layout((void*)win);

    mu_assert("Children are resized (1w)", label1->widget.width == 200);
    mu_assert("Children are resized (1h)", label1->widget.height == 0);
    mu_assert("Children are resized (2w)", label2->widget.width == 400);
    mu_assert("Children are resized (2h)", label2->widget.height == 0);

    mu_assert("Children are positioned (1w)", label1->widget.x == 0);
    mu_assert("Children are positioned (1h)", label1->widget.y == 0);
    mu_assert("Children are positioned (2w)", label2->widget.x == 200);
    mu_assert("Children are positioned (2h)", label2->widget.y == 0);

    return NULL;
}

char* test_uik_flex_layout_extend_vertical() {
    struct uik_window* win = uik_window_new("win", 600, 300, 0, 0);

    struct uik_flex* flex = uik_flex_new("flex", VERTICAL);
    uik_window_pack(win, (void*)flex);

    struct uik_label* label1 = uik_label_new("label1", "test1");
    uik_flex_pack(flex, (void*)label1, 1);
    struct uik_label* label2 = uik_label_new("label2", "test2");
    uik_flex_pack(flex, (void*)label2, 2);

    _uik_layout((void*)win);

    mu_assert("Children are resized (1w)", label1->widget.width == 0);
    mu_assert("Children are resized (1h)", label1->widget.height == 100);
    mu_assert("Children are resized (2w)", label2->widget.width == 0);
    mu_assert("Children are resized (2h)", label2->widget.height == 200);

    mu_assert("Children are positioned (1w)", label1->widget.x == 0);
    mu_assert("Children are positioned (1h)", label1->widget.y == 0);
    mu_assert("Children are positioned (2w)", label2->widget.x == 0);
    mu_assert("Children are positioned (2h)", label2->widget.y == 100);

    return NULL;
}