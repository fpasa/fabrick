#include "uik.h"

void _uik_render(struct uik_widget* widget, struct uik_window* win) {
    switch (widget->class) {
        case WINDOW:
            _uik_window_render((struct uik_window*)widget, win);
            break;
        case FLEX:
            _uik_flex_render((struct uik_flex*)widget, win);
            break;
        case LABEL:
            _uik_label_render((struct uik_label*)widget, win);
            break;
    }
}