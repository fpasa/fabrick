#include "uik.h"

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include "ut/uthash.h"
#include "protocol/protocol.h"

struct _uik_window_todo_item* _uik_window_todo_list;

struct uik_window* uik_window_new(char* name, uint16_t width, uint16_t height, uint16_t x, uint16_t y) {
    struct uik_window* win = malloc(sizeof(struct uik_window));

    win->widget.class = WINDOW;
    win->widget.parent = NULL;
    win->widget.children = NULL;

    win->type = BUFFER_NORMAL;
    win->width = width;
    win->height = height;
    win->x = x;
    win->y = y;
    
    uik_add_obj(name, win);

    // Add todo list item, so that the mainloop
    // creates a window on the next loop
    struct _uik_window_todo_item* item = malloc(sizeof(struct _uik_window_todo_item));
    item->type = CREATE_WINDOW;
    item->win_id = malloc(strlen(name)+1);
    strcpy(item->win_id, name);
    HASH_ADD_STR(_uik_window_todo_list, win_id, item);

    return win;
}

void uik_window_clear(struct uik_window* win, uint32_t color) {
    for (uint16_t y = 0; y < win->height; ++y) {
        for (uint16_t x = 0; x < win->width; ++x) {
            win->_p[y * win->width + x] = color;
        }
    }
}


void uik_window_pack(struct uik_window* win, struct uik_widget* child) {
    if (win->widget.children != NULL) {
        win->widget.children->parent = NULL;
    }

    win->widget.children = child;
    child->parent = (struct uik_widget*)win;
}

struct _uik_geometry _uik_window_layout_size(struct uik_window* win, struct _uik_constrains* constrains) {
    if (win->widget.children != NULL) {
        struct _uik_constrains con = {
            .max_width = win->width,
            .max_height = win->height,
            .min_width = win->width,
            .min_height = win->height,
        };
        struct _uik_geometry child_geo = _uik_layout_size(win->widget.children, &con);
        // Save geometry in the widget node
        win->widget.children->width = child_geo.width;
        win->widget.children->height = child_geo.height;
    }

    struct _uik_geometry geo = {
        .width = win->width,
        .height = win->height,
    };
    win->widget.width = win->width;
    win->widget.height = win->height;
    return geo;
}

void _uik_window_layout_position(struct uik_window* win) {
    win->widget.x = 0;
    win->widget.y = 0;
    if (win->widget.children != NULL) { 
        win->widget.children->x = 0;
        win->widget.children->y = 0;
        _uik_layout_position(win->widget.children);
    }
}

void _uik_render(struct uik_window* win, struct uik_window* _) {
    win->_p 
}