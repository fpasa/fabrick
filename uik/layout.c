#include "uik.h"

#include "ut/utlist.h"

void _uik_layout(struct uik_widget* widget) {
    _uik_layout_size(widget, NULL);
    _uik_layout_position(widget);
}

struct _uik_geometry _uik_layout_size(struct uik_widget* widget, struct _uik_constrains* constrains) {
    switch (widget->class) {
        case WINDOW:
            return _uik_window_layout_size((struct uik_window*)widget, constrains);
        case FLEX:
            return _uik_flex_layout_size((struct uik_flex*)widget, constrains);
        case LABEL:
            return _uik_label_layout_size((struct uik_label*)widget, constrains);
    }
}

void _uik_layout_position(struct uik_widget* widget) {
    switch (widget->class) {
        case WINDOW:
            _uik_window_layout_position((struct uik_window*)widget);
            break;
        case FLEX:
            _uik_flex_layout_position((struct uik_flex*)widget);
            break;
        case LABEL:
            _uik_label_layout_position((struct uik_label*)widget);
            break;
    }
}