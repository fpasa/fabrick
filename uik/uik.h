#ifndef UIK_MAINLOOP_H
#define UIK_MAINLOOP_H

#include "ut/uthash.h"
#include "ut/utlist.h"
#include "protocol/protocol.h"

// Mainloop
// ----------------------------------------
int uik_mainloop();

// Object registry
// ----------------------------------------
void uik_add_obj(char* id, void* obj);
void uik_del_obj(char* id);
void uik_get_obj(char* id, void** obj);

// Widget
// ----------------------------------------
enum _uik_widget_class {
    WINDOW,
    FLEX,
    LABEL,
};

// Widgets form a tree
struct uik_widget {
    enum _uik_widget_class class;
    struct uik_widget* parent;
    struct uik_widget* children;
    struct uik_widget *prev, *next;
    // Layout
    uint32_t width, height;
    uint32_t x, y;
};

// Layout
// ----------------------------------------
struct _uik_constrains {
    uint32_t max_width, max_height;
    uint32_t min_width, min_height;
};

struct _uik_geometry {
    uint32_t width;
    uint32_t height;
};

void _uik_layout(struct uik_widget* widget);
struct _uik_geometry _uik_layout_size(struct uik_widget* widget, struct _uik_constrains* constrains);
void _uik_layout_position(struct uik_widget* widget, struct uik_window* win);

// Render
// ----------------------------------------
void _uik_render(struct uik_widget* widget);

// Windows
// ----------------------------------------
// private
enum _uik_window_todo_type {
    CREATE_WINDOW,
    RESIZE_WINDOW,
    MOVE_WINDOW,
    CLOSE_WINDOW,
};

struct _uik_window_todo_item {
	UT_hash_handle hh;
    enum _uik_window_todo_type type;
    char* win_id;
};
extern struct _uik_window_todo_item* _uik_window_todo_list;

// public
struct uik_window {
    struct uik_widget widget;
    uint8_t type;
    uint16_t width, height;
    uint16_t x, y;
    // private
    char _buf_name[BUF_NAME_LENGTH];
    uint32_t* _p;
    uint32_t _size;
};

struct uik_window* uik_window_new(char* name, uint16_t width, uint16_t height, uint16_t x, uint16_t y);

void uik_window_clear(struct uik_window* win, uint32_t color);

void uik_window_pack(struct uik_window* win, struct uik_widget* child);

struct _uik_geometry _uik_window_layout_size(struct uik_window* win, struct _uik_constrains* constrains);
void _uik_window_layout_position(struct uik_window* win);
void _uik_window_render(struct uik_window* win, struct uik_window* _);

// Layout widgets
// ----------------------------------------
enum uik_flex_orientation {
    HORIZONTAL,
    VERTICAL,
};

struct uik_flex {
    struct uik_widget widget;
    enum uik_flex_orientation orientation;
    // private
    uint8_t children_expand[32];
};

struct uik_flex* uik_flex_new(char* name, enum uik_flex_orientation orientation);

void uik_flex_pack(struct uik_flex* flex, struct uik_widget* child, uint8_t expand);

struct _uik_geometry _uik_flex_layout_size(struct uik_flex* flex, struct _uik_constrains* constrains);
void _uik_flex_layout_position(struct uik_flex* flex);
void _uik_flex_render(struct uik_flex* flex, struct uik_window* win);

// Label widget
// ----------------------------------------
struct uik_label {
    struct uik_widget widget;
    char* text;
};

struct uik_label* uik_label_new(char* name, char* text);

struct _uik_geometry _uik_label_layout_size(struct uik_label* label, struct _uik_constrains* constrains);
void _uik_label_layout_position(struct uik_label* label);
void _uik_label_render(struct uik_label* label, struct uik_window* win);

#endif