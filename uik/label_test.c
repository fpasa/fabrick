#include "test/minunit.h"

#include "uik.h"

char* test_uik_label_new() {
    struct uik_label* label = uik_label_new("label", "Test");

    mu_assert("created label widget has class 'label'",
        label->widget.class == LABEL);

    struct uik_label* reg_label = NULL;
    uik_get_obj("label", (void **)&reg_label);
    mu_assert("created label widget is added to the registry",
        reg_label == label);

    return NULL;
}