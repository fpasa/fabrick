#include "uik.h"

struct uik_flex* uik_flex_new(char* name, enum uik_flex_orientation orientation) {
    struct uik_flex* flex = malloc(sizeof(struct uik_flex));

    flex->widget.class = FLEX;
    flex->widget.parent = NULL;
    flex->widget.children = NULL;

    flex->orientation = orientation;

    uik_add_obj(name, flex);

    return flex;
}

void uik_flex_pack(struct uik_flex* flex, struct uik_widget* child, uint8_t expand) {
    uint32_t count;
    struct uik_widget* w;
    DL_COUNT(flex->widget.children, w, count);

    if (count < 32) {
        DL_APPEND(flex->widget.children, child);
        flex->children_expand[count] = expand;
    }
}

struct _uik_geometry _uik_flex_layout_size(struct uik_flex* flex, struct _uik_constrains* constrains) {
    uint8_t i = 0;
    struct uik_widget* w;

    int is_horizontal = flex->orientation == HORIZONTAL;
    uint32_t size_along = is_horizontal ?
        constrains->min_width : constrains->min_height;

    // STEP 1: compute size and layout non-expandable children
    // -------------------------------------------------------
    i = 0;
    // We also compute the cumulative and max dimensions along
    // the orientation and across
    uint32_t cum_along = 0;
    uint32_t max_across = 0;
    DL_FOREACH(flex->widget.children, w) {
        uint8_t expand = flex->children_expand[i++];
        if (!expand) {
            // These are non-expandable widgets 
            struct _uik_constrains con = {
                .max_width = constrains->max_width,
                .max_height = constrains->max_height,
                .min_width = 0,
                .min_height = 0,
            };

            // Compute geometry and save it in the
            struct _uik_geometry child_geo = _uik_layout_size(w, &con);
            // Save geometry in the widget node
            w->width = child_geo.width;
            w->height = child_geo.height;

            cum_along += is_horizontal ?
                child_geo.width : child_geo.height;

            // Update max size across if necessary
            uint32_t across = is_horizontal ?
                child_geo.height : child_geo.width;
            if (across > max_across) max_across = across;
        }
    }

    // Compute total expand weight
    uint32_t count = i; // i is now equal to the children count
    uint32_t total_expand = 0;
    for (i = 0; i < count; ++i) total_expand += flex->children_expand[i];

    // Compute total expand space
    uint32_t expand_size = (is_horizontal ?
        constrains->min_width : constrains->min_height) - cum_along;

    // STEP 2: layout expandable children to fill remaining space
    // ----------------------------------------------------------
    i = 0;
    DL_FOREACH(flex->widget.children, w) {
        uint8_t expand = flex->children_expand[i++];
        if (expand) {
            // These expandable widgets

            // Compute the size along orientation according to expand weight
            uint32_t size_along = ((float)expand / total_expand) * expand_size;
            
            // Force child to be of the right size
            struct _uik_constrains con = {
                .max_width = is_horizontal ? size_along : constrains->max_width,
                .max_height = is_horizontal ? constrains->max_height : size_along,
                .min_width = is_horizontal ? size_along : 0,
                .min_height = is_horizontal ? 0 : size_along,
            };

            struct _uik_geometry child_geo = _uik_layout_size(w, &con);
            // Save geometry in the widget node
            w->width = child_geo.width;
            w->height = child_geo.height;

            // Update max size across if necessary
            uint32_t across = is_horizontal ?
                child_geo.height : child_geo.width;
            if (across > max_across) max_across = across;
        }
    }

    // STEP 3: finally set the dimension of the container
    // --------------------------------------------------
    // The geometry of the container is:
    // - orientation direction -> min
    // - other direction -> fit content but respect min and max
    struct _uik_geometry geo = {
        .width = is_horizontal ?
            constrains->min_width : 0,
        .height = is_horizontal ?
            0 : constrains->min_height,
    };
    
    return geo;
}

void _uik_flex_layout_position(struct uik_flex* flex) {
    struct uik_widget* w;

    uint32_t x = 0;
    uint32_t y = 0;
    DL_FOREACH(flex->widget.children, w) {
        w->x = x;
        w->y = y;

        if (flex->orientation == HORIZONTAL) {
            x += w->width;
        } else {
            y += w->height;
        }

        _uik_layout_position(w);
    }
}

void _uik_flex_render((struct uik_flex*)widget) {
    
}