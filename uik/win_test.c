#include "test/minunit.h"

#include "uik.h"

char* test_uik_window_pack() {
    struct uik_window* win = uik_window_new("win", 640, 480, 0, 0);

    struct uik_label* label1 = uik_label_new("label1", "test1");
    struct uik_label* label2 = uik_label_new("label2", "test2");

    uik_window_pack(win, (void*)label1);
    mu_assert("Parent->child created",
        win->widget.children == (struct uik_widget*)label1);
    mu_assert("Child->parent created",
        label1->widget.parent == (struct uik_widget*)win);

    uik_window_pack(win, (void*)label2);
    mu_assert("Old parent->child destroyed",
        win->widget.children == (struct uik_widget*)label2);
    mu_assert("Old child->parent destroyed",
        label1->widget.parent == NULL);


    return NULL;
}

char* test_uik_window_layout() {
    struct uik_window* win = uik_window_new("win", 640, 480, 0, 0);

    struct uik_label* label = uik_label_new("label", "test");
    uik_window_pack(win, (void*)label);

    _uik_layout((void*)win);

    mu_assert("Window sizes child width correctly",
        label->widget.width == 640);
    mu_assert("Window sizes child height correctly",
        label->widget.height == 480);

    mu_assert("Window positions child on the left",
        label->widget.x == 0);
    mu_assert("Window positions child on the topy",
        label->widget.y == 0);

    return NULL;
}