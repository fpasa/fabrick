#include <stdio.h>
#include "test/minunit.h"

int tests_run = 0;

static char * all_tests() {
    // Window
    mu_run_test(test_uik_window_pack);
    mu_run_test(test_uik_window_layout);
    // Flex
    mu_run_test(test_uik_flex_new);
    mu_run_test(test_uik_flex_layout_no_extend);
    mu_run_test(test_uik_flex_layout_extend);
    mu_run_test(test_uik_flex_layout_extend_vertical);
    // Label
    mu_run_test(test_uik_label_new);

    return 0;
}

int main() {
    char *result = all_tests();
    if (result != 0) {
        printf("%s\n", result);
    } else {
        printf("ALL TESTS PASSED\n");
    }

    printf("Tests run: %d\n", tests_run);

    return result != 0;
}