#ifndef TEST_H
#define TEST_H

 #define mu_assert(message, test) do { if (!(test)) return message; } while (0)
 #define mu_run_test(test) \
    char *test(); \
    do { char *message = test(); tests_run++; \
    if (message) { printf("[FAILED] %s\n", #test); return message; } } while (0)
 extern int tests_run;

#endif